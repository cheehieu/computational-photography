ó
½3Vc           @   ss   d  d l  Z d  d l Z d  d l Z d  d l Z d   Z d   Z d   Z d   Z	 d   Z
 d   Z d   Z d S(	   iÿÿÿÿNc         C   s>   t  j d |  d d |  d d |  d g  } t  j | |  S(   sã    Return a 5x5 generating kernel based on an input parameter.

  Note: This function is provided for you, do not change it.

  Args:
    parameter (float): Range of value: [0, 1].

  Returns:
    numpy.ndarray: A 5x5 kernel.

  g      Ð?g       @(   t   npt   arrayt   outer(   t	   parametert   kernel(    (    sI   /mnt/hgfs/GitHub/computational-photography/assignments/hw8/assignment6.pyt   generatingKernel!   s    c         C   sM   t  d  } t j j |  | d d } | d d d  d d d  f } | S(   s   Convolve the input image with a generating kernel of parameter of 0.4 and
  then reduce its width and height by two.

  Please consult the lectures and readme for a more in-depth discussion of how
  to tackle the reduce function.

  You can use any / all functions to convolve and reduce the image, although
  the lectures have recommended methods that we advise since there are a lot
  of pieces to this assignment that need to work 'just right'.

  Args:
    image (numpy.ndarray): a grayscale image of shape (r, c)

  Returns:
    output (numpy.ndarray): an image of shape (ceil(r/2), ceil(c/2))
      For instance, if the input is 5x7, the output will be 3x4.

  gÙ?t   modet   sameNi   (   R   t   scipyt   signalt
   convolve2d(   t   imageR   t	   convolvedt   reduced(    (    sI   /mnt/hgfs/GitHub/computational-photography/assignments/hw8/assignment6.pyt   reduce1   s    "c         C   s   t  j d t |   d t |  d  f  } |  | d d d  d d d  f <t d  } t j j | | d d } | d } | S(   s¿   Expand the image to double the size and then convolve it with a generating
  kernel with a parameter of 0.4.

  You should upsample the image, and then convolve it with a generating kernel
  of a = 0.4.

  Finally, multiply your output image by a factor of 4 in order to scale it
  back up. If you do not do this (and I recommend you try it out without that)
  you will see that your images darken as you apply the convolution. Please
  explain why this happens in your submission PDF.

  Please consult the lectures and readme for a more in-depth discussion of how
  to tackle the expand function.

  You can use any / all functions to convolve and reduce the image, although
  the lectures have recommended methods that we advise since there are a lot
  of pieces to this assignment that need to work 'just right'.

  Args:
    image (numpy.ndarray): a grayscale image of shape (r, c)

  Returns:
    output (numpy.ndarray): an image of shape (2*r, 2*c)
  i   i    NgÙ?R   R   i   (   R    t   zerost   lenR   R   R	   R
   (   R   t	   upsampledR   R   t   expanded(    (    sI   /mnt/hgfs/GitHub/computational-photography/assignments/hw8/assignment6.pyt   expandN   s    -"
c         C   s;   |  g } x+ t  |  D] } | j t | |   q W| S(   s:   Construct a pyramid from the image by reducing it by the number of levels
  passed in by the input.

  Note: You need to use your reduce function in this function to generate the
  output.

  Args:
    image (numpy.ndarray): A grayscale image of dimension (r,c) and dtype float.
    levels (uint8): A positive integer that specifies the number of reductions
                    you should do. So, if levels = 0, you should return a list
                    containing just the input image. If levels = 1, you should
                    do one reduction. len(output) = levels + 1

  Returns:
    output (list): A list of arrays of dtype np.float. The first element of the
                   list (output[0]) is layer 0 of the pyramid (the image
                   itself). output[1] is layer 1 of the pyramid (image reduced
                   once), etc. We have already included the original image in
                   the output array for you. The arrays are of type
                   numpy.ndarray.

  Consult the lecture and README for more details about Gaussian Pyramids.
  (   t   xranget   appendR   (   R   t   levelst   outputt   i(    (    sI   /mnt/hgfs/GitHub/computational-photography/assignments/hw8/assignment6.pyt   gaussPyramidw   s    	c         C   s  g  } xÝ t  t |   d  D]Å } t |  | d  } t |  t |  |  k r| | d t |  |   d d  f } n  t | d  t |  | d  k rÍ | d d  d t |  | d   f } n  | j |  | |  q W| j |  t |   d  | S(   s1   Construct a Laplacian pyramid from the Gaussian pyramid, of height levels.

  Note: You must use your expand function in this function to generate the
  output. The Gaussian Pyramid that is passed in is the output of your
  gaussPyramid function.

  Args:
    gaussPyr (list): A Gaussian Pyramid as returned by your gaussPyramid
                     function. It is a list of numpy.ndarray items.

  Returns:
    output (list): A Laplacian pyramid of the same size as gaussPyr. This
                   pyramid should be represented in the same way as guassPyr, 
                   as a list of arrays. Every element of the list now
                   corresponds to a layer of the Laplacian pyramid, containing
                   the difference between two layers of the Gaussian pyramid.

           output[k] = gauss_pyr[k] - expand(gauss_pyr[k + 1])

           Note: The last element of output should be identical to the last 
           layer of the input pyramid since it cannot be subtracted anymore.

  Note: Sometimes the size of the expanded image will be larger than the given
  layer. You should crop the expanded image to match in shape with the given
  layer.

  For example, if my layer is of size 5x7, reducing and expanding will result
  in an image of size 6x8. In this case, crop the expanded layer to 5x7.
  i   i    N(   R   R   R   R   (   t   gaussPyrR   R   R   (    (    sI   /mnt/hgfs/GitHub/computational-photography/assignments/hw8/assignment6.pyt   laplPyramid   s     )$-c         C   sT   g  } xG t  t |   D]3 } | j | | |  | d | | | |  q W| S(   s   Blend the two Laplacian pyramids by weighting them according to the
  Gaussian mask.

  Args:
    laplPyrWhite (list): A Laplacian pyramid of one image, as constructed by
                         your laplPyramid function.

    laplPyrBlack (list): A Laplacian pyramid of another image, as constructed by
                         your laplPyramid function.

    gaussPyrMask (list): A Gaussian pyramid of the mask. Each value is in the
                         range of [0, 1].

  The pyramids will have the same number of levels. Furthermore, each layer
  is guaranteed to have the same shape as previous levels.

  You should return a Laplacian pyramid that is of the same dimensions as the 
  input pyramids. Every layer should be an alpha blend of the corresponding
  layers of the input pyramids, weighted by the Gaussian mask. This means the
  following computation for each layer of the pyramid:
    output[i, j] = current_mask[i, j] * white_image[i, j] + 
                   (1 - current_mask[i, j]) * black_image[i, j]
  Therefore:
    Pixels where current_mask == 1 should be taken completely from the white
    image.
    Pixels where current_mask == 0 should be taken completely from the black
    image.

  Note: current_mask, white_image, and black_image are variables that refer to
  the image in the current layer we are looking at. You do this computation for
  every layer of the pyramid.
  i   (   R   R   R   (   t   laplPyrWhitet   laplPyrBlackt   gaussPyrMaskt   blended_pyrR   (    (    sI   /mnt/hgfs/GitHub/computational-photography/assignments/hw8/assignment6.pyt   blendÊ   s    $1c         C   s  |  t  |   d } xè t t  |   d d d  D]Ê } t |  } t  |  t  |  | d  k r | d t  |  | d   d d  f } n  t  | d  t  |  | d d  k ré | d d  d t  |  | d d   f } n  | |  | d } q1 W| S(   sÝ   Collapse an input pyramid.

  Args:
    pyramid (list): A list of numpy.ndarray images. You can assume the input is
                  taken from blend() or laplPyramid().

  Returns:
    output(numpy.ndarray): An image of the same shape as the base layer of the
                           pyramid and dtype float.

  Approach this problem as follows, start at the smallest layer of the pyramid.
  Expand the smallest layer, and add it to the second to smallest layer. Then,
  expand the second to smallest layer, and continue the process until you are
  at the largest image. This is your result.

  Note: sometimes expand will return an image that is larger than the next
  layer. In this case, you should crop the expanded image down to the size of
  the next layer. Look into numpy slicing / read our README to do this easily.

  For example, expanding a layer of size 3x4 will result in an image of size
  6x8. If the next layer is of size 5x7, crop the expanded image to size 5x7.
  i   i    iÿÿÿÿN(   R   R   R   (   t   pyramidR   R   R   (    (    sI   /mnt/hgfs/GitHub/computational-photography/assignments/hw8/assignment6.pyt   collapseô   s    # -(1(   t   numpyR    R   t   spt   scipy.signalt   cv2R   R   R   R   R   R    R"   (    (    (    sI   /mnt/hgfs/GitHub/computational-photography/assignments/hw8/assignment6.pyt   <module>   s   			)	 	3	*